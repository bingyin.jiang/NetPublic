﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UpLoadIDPHelper
{
    public class CbxItem
    {
        public string value { get; set; }
        public string name { get; set; }

        public override string ToString()
        {
            return name;
        }
    }
}
