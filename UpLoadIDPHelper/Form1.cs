﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UpLoadIDPHelper
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private SqlConnection GetSqlConnection()
        {
            return new SqlConnection(this.tbxConnect.Text);
        }

        private DataTable Query(string sql)
        {
            SqlConnection Connection = GetSqlConnection();

            SqlCommand command = new SqlCommand(sql);
            command.Connection = Connection;

            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
            DataSet ds = new DataSet();
            sqlDataAdapter.Fill(ds);
            Connection.Close();
            return ds.Tables[0];
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string connect = "Data Source=172.16.2.2;Initial Catalog=EDevelopment;Persist Security Info=True;User ID=EDevelopment;Password=Sanofi2011";
            this.tbxConnect.Text = connect;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoadYears();

            this.groupBox1.Enabled = true;
        }

        #region 载入有启动了项目的年份
        /// <summary>
        /// 载入有启动了项目的年份
        /// </summary>
        private void LoadYears()
        {
            string sql = "SELECT DISTINCT SUBSTRING(Code, 1, 4) FROM ED_PROGRAM_INFO";
            DataTable dt = Query(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    this.cbxYears.Items.Add(dr[0] as string);
                }

                this.cbxYears.SelectedIndex = 0;
            }

        }
        #endregion

        #region 载入指定年份的项目
        /// <summary>
        /// 载入指定年份的项目
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            string year = cbxYears.Text;
            string sql = "SELECT * FROM ED_PROGRAM_INFO WHERE Code LIKE '" + year + "%'";
            DataTable dt = Query(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                this.cbxPrograms.Items.Clear();
                foreach (DataRow dr in dt.Rows)
                {
                    CbxItem cbxItem = new CbxItem();
                    cbxItem.value = Convert.ToString(dr["ID"]);
                    cbxItem.name = dr["NameCN"] as string;

                    this.cbxPrograms.Items.Add(cbxItem);
                }

                this.cbxPrograms.SelectedIndex = 0;

            }
        }
        #endregion

        #region 检查按钮
        /// <summary>
        /// 检查按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCheck_Click(object sender, EventArgs e)
        {
            this.richTextBox1.Clear();
            StringBuilder sb = new StringBuilder();

            string email = this.tbxEmail.Text;

            string userId = string.Empty;
            string programId = (this.cbxPrograms.SelectedItem as CbxItem).value;
            string groupId = string.Empty;
            string milestoneId = string.Empty;
            DateTime milestoneDate;


            // 查找用户信息
            string sql = "SELECT * FROM ED_USER WHERE EMail='" + email + "'";
            DataTable dt = Query(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                userId = Convert.ToString(dr["ID"]);
                sb.AppendLine("查到用户");
                sb.AppendLine(string.Format("\t姓名：[中文]{0}\t[英文]{1}", dr["NameCN"], dr["NameEN"]));
                sb.AppendLine(string.Format("\t密码： {0}", CryptographyHelper.DeCryptor(dr["Password"] as string)));
                this.richTextBox1.Text = sb.ToString();
            }
            else
            {
                MessageBox.Show("无此学员", "错误提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // 查找参与组信息
            sql = "SELECT * FROM ED_PARTICIPANT_GROUP WHERE UserID='#UserID#' AND ProgramID='#ProgramID#' ";
            dt = Query(sql.Replace("#UserID#", userId).Replace("#ProgramID#", programId));
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                groupId = Convert.ToString(dr["GroupID"]);
                sb.AppendLine("查到项目");
                sb.AppendLine("\t项目名称：" + this.cbxPrograms.Text);
            }
            else
            {
                MessageBox.Show("此学员在指定项目中未加入组", "错误提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            sql = "SELECT * FROM ED_GROUP WHERE ID='" + groupId + "'";
            dt = Query(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                sb.AppendLine(string.Format("\t组名：[中文]{0}\t[英文]{1}", dr["NameCN"], dr["NameEN"]));
            }
            else
            {
                MessageBox.Show("查询此学员在所在组信息失败", "错误提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // 找到组里程碑时间点
            sql = "SELECT * FROM ED_GROUP_MILESTONE WHERE (State=21 OR State=22) AND GroupID='" + groupId + "' ORDER BY State ";
            dt = Query(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                milestoneId = Convert.ToString(dr["ID"]);
                milestoneDate = Convert.ToDateTime(dr["MilestoneDate"]);

                sb.AppendLine(string.Format("\tIDP 截止日期：{0}", milestoneDate));
            }
            else
            {
                MessageBox.Show("查询此学员在所在组的IDP任务点失败", "错误提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // 判断是否存在IDP
            sql = "SELECT * FROM ED_IDP WHERE UserID='#UserID#' AND ProgramID='#ProgramID#' AND GroupID='#GroupID#' ";
            sql = sql.Replace("#UserID#", userId);
            sql = sql.Replace("#ProgramID#", programId);
            sql = sql.Replace("#GroupID#", groupId);
            dt = Query(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                string idpId = Convert.ToString(dr["ID"]);

                string _sql = "UPDATE ED_IDP SET ApproveStatus=2 WHERE ID='" + idpId + "' ";
                sb.AppendLine("已经存在一条IDP，请执行更新命令：");
                this.richTextBox2.Text = _sql;
            }
            else
            {
                string _sql = "INSERT INTO ED_IDP (ID, UserID, ProgramID, GroupID, UploadIDPTime, ApproveStatus, GroupMilestoneID, Creator, CreateDate, ModifyUser, ModifyDate) "
                    + " VALUES (NEWID(), '#UserID#', '#ProgramID#', '#GroupID#', '#MilestoneDate#', 2, '#MilestoneID#', '#UserID#',GETDATE(),'#UserID#', GETDATE()) ";
                _sql = _sql.Replace("#UserID#", userId);
                _sql = _sql.Replace("#ProgramID#", programId);
                _sql = _sql.Replace("#GroupID#", groupId);
                _sql = _sql.Replace("#MilestoneID#", milestoneId);
                _sql = _sql.Replace("#MilestoneDate#", milestoneDate.ToString("yyyy-MM-dd HH:mm:ss"));

                sb.AppendLine("不存在IDP，请执行新建命令：");
                this.richTextBox2.Text = _sql;
            }
            
            this.richTextBox1.Text = sb.ToString();
        }
        #endregion

        #region 执行按钮
        /// <summary>
        /// 执行按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            string sql = this.richTextBox2.Text;
            if (!string.IsNullOrEmpty(sql))
            {
                SqlConnection Connection = GetSqlConnection();
                Connection.Open();
                SqlCommand command = new SqlCommand(sql);
                command.Connection = Connection;

                if (command.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("执行SQL成功", "错误提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.richTextBox2.Clear();
                }
                else
                {
                    MessageBox.Show("执行SQL失败", "错误提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Connection.Close();
            }
        }
        #endregion


    }
}
