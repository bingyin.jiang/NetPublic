﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace UpLoadIDPHelper
{
    public class CryptographyHelper
    {
        #region 字符串加密

        private static readonly string KEY = "l&*^yjYEG,.e)dF8";

        private static readonly string IV = "g8iYoDmC";


        /// <summary>
        /// 加密字符串
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string EnCryptor(string text)
        {
            SymmetricAlgorithm provider = SymmetricAlgorithm.Create("TripleDES");

            provider.Key = Encoding.Default.GetBytes(KEY);

            provider.IV = Encoding.Default.GetBytes(IV);

            ICryptoTransform encryptor = provider.CreateEncryptor();

            byte[] bytIn = UTF8Encoding.Default.GetBytes(text);

            MemoryStream ms = new MemoryStream();

            CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write);
            cs.Write(bytIn, 0, bytIn.Length);
            cs.FlushFinalBlock();
            ms.Close();
            byte[] bytOut = ms.ToArray();

            return Convert.ToBase64String(bytOut);
        }

        /// <summary>
        /// 解密字符串
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string DeCryptor(string text)
        {
            SymmetricAlgorithm provider = SymmetricAlgorithm.Create("TripleDES");

            provider.Key = Encoding.Default.GetBytes(KEY);

            provider.IV = Encoding.Default.GetBytes(IV);

            ICryptoTransform decryptor = provider.CreateDecryptor();

            byte[] bytIn = Convert.FromBase64String(text);

            MemoryStream ms = new MemoryStream(bytIn, 0, bytIn.Length);

            CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read);

            StreamReader sr = new StreamReader(cs);

            return sr.ReadToEnd();
        }

        #endregion

        #region 文件加密
        /// <summary>
        /// 文件加密
        /// </summary>
        /// <param name="inFileName">需要加密的文件(文件的完整路径)</param>
        /// <param name="outFileName">加密后的文件(文件的完整路径)</param>
        /// <param name="sAlgorithm">对称算法实例</param>
        /// <returns>bool</returns>
        public static bool EncryptFile(string InFileName, string OutFileName, SymmetricAlgorithm sAlgorithm)
        {
            //将文件内容读取到字节数组
            FileStream inFileStream = new FileStream(InFileName, FileMode.Open, FileAccess.Read);
            byte[] sourceByte = new byte[inFileStream.Length];
            inFileStream.Read(sourceByte, 0, sourceByte.Length);
            inFileStream.Flush();
            inFileStream.Close();

            MemoryStream encryptStream = new MemoryStream();
            CryptoStream encStream = new CryptoStream(encryptStream, sAlgorithm.CreateEncryptor(), CryptoStreamMode.Write);
            try
            {
                //利用链接流加密源字节数组
                encStream.Write(sourceByte, 0, sourceByte.Length);
                encStream.FlushFinalBlock();

                //将字节数组信息写入指定文件
                FileStream outFileStream = new FileStream(OutFileName, FileMode.OpenOrCreate, FileAccess.Write);
                BinaryWriter bWriter = new BinaryWriter(outFileStream);
                bWriter.Write(encryptStream.ToArray());
                encryptStream.Flush();

                bWriter.Close();
                encryptStream.Close();
            }
            catch (Exception error)
            {
                throw (error);
            }
            finally
            {
                encryptStream.Close();
                encStream.Close();
            }
            return true;
        }

        /// <summary>
        /// 文件解密
        /// </summary>
        /// <param name="inFileName">需要解密的文件(文件的完整路径)</param>
        /// <param name="outFileName">解密后的文件(文件的完整路径)</param>
        /// <param name="sAlgorithm">对称算法实例</param>
        /// <returns>bool</returns>
        public static bool DecryptFile(string InFileName, string OutFileName, SymmetricAlgorithm sAlgorithm)
        {

            //读取被加密文件到字节数组
            FileStream encryptFileStream = new FileStream(InFileName, FileMode.Open, FileAccess.Read);
            byte[] encryptByte = new byte[encryptFileStream.Length];
            encryptFileStream.Read(encryptByte, 0, encryptByte.Length);
            encryptFileStream.Flush();
            encryptFileStream.Close();

            MemoryStream decryptStream = new MemoryStream();
            CryptoStream encStream = new CryptoStream(decryptStream, sAlgorithm.CreateDecryptor(), CryptoStreamMode.Write);
            try
            {
                encStream.Write(encryptByte, 0, encryptByte.Length);
                encStream.FlushFinalBlock();

                byte[] decryptByte = decryptStream.ToArray();
                FileStream decryptFileStream = new FileStream(OutFileName, FileMode.OpenOrCreate, FileAccess.Write);
                BinaryWriter bWriter = new BinaryWriter(decryptFileStream, Encoding.Default);
                bWriter.Write(decryptByte);
                decryptFileStream.Flush();

                bWriter.Close();
                decryptFileStream.Close();
            }
            catch (Exception error)
            {
                throw (error);
            }
            finally
            {
                decryptStream.Close();
                encStream.Close();
            }

            return true;
        }

        #endregion
    }
}
